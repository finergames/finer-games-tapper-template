﻿using UnityEngine;
using System.Collections;

public class ReplayLevel : MonoBehaviour, iInputManager {

	#region iInputManager implementation
	public void OnTouchBegan ()
	{
		//throw new System.NotImplementedException ();
	}
	public void OnTouchEnded ()
	{
		Grid.levelManager.sceneToLoad = "Level";
		Grid.levelManager.sceneEnding = true;
	}
	public void OnTouchMoved ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchStayed ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchBeganAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchEndedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchMovedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	public void OnTouchStayedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}
	#endregion

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
