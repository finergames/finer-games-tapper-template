﻿using UnityEngine;
using System.Collections;

public class TouchLogicV2 : MonoBehaviour {
	
	public static int currTouch = 0;//so other scripts can know what touch is currently on screen
	private int touch2Watch = 64;
	private Ray ray;//this will be the ray that we cast from our touch into the scene
	private RaycastHit rayHitInfo = new RaycastHit();//return the info of the object that was hit by the ray
	private iInputManager touchedObject = null;
	private iInputManager firstTouchedObject = null;
//	private iInputManager clickedObject = null;
	private iInputManager firstClickedObject = null;
	
	public bool isTouchDevice = false;
	public static Vector3 touchPosition;
	
	private bool clicked = false;
	
	public void Awake(){
		if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android) 
			isTouchDevice = true; 
		else
			isTouchDevice = false; 
	}
	
	public void Update() {
		/*
		 * FOR DEBUGGING TOUCHLOGIC. IT'S STILL REALLY FRAGILE, SO PLEASE DON'T DELETE
		 */

//		Debug.Log ("first touched object is " + firstTouchedObject);
//		Debug.Log ("touched object is " + touchedObject);
//		Debug.Log ("first clicked object is " + firstClickedObject);
//		Debug.Log ("clicked object is " + clickedObject);

		
		//detect click and calculate touch position
		if (isTouchDevice)
		{
			touchPosition = Input.GetTouch(0).position;
		} else {
			touchPosition = Input.mousePosition;
		}
		
		ray = Camera.main.ScreenPointToRay(touchPosition);//creates ray from screen point position
		
		//if the ray hit something, only if it hit something
		
		if (Physics.Raycast (ray, out rayHitInfo)) {
			//if the thing that was hit implements iInputManager
			touchedObject = rayHitInfo.transform.GetComponent (typeof(iInputManager)) as iInputManager;
		} else {
			touchedObject = null;	
		}


		if(isTouchDevice){
			Debug.Log("touch device");

			for(int i = 0; i < Input.touchCount; i++)
			{
				currTouch = i;
				
				//if the ray hit something and it implements iInputManager
				if (touchedObject != null)
				{
					switch(Input.GetTouch(i).phase)
					{
					case TouchPhase.Began:
						firstTouchedObject = touchedObject;
						firstTouchedObject.OnTouchBegan();
						touch2Watch = currTouch;
						break;
					case TouchPhase.Ended:
						if(firstTouchedObject != null && firstTouchedObject == touchedObject)
						{
							firstTouchedObject.OnTouchEnded();
						}
						firstTouchedObject = null;
						touchedObject = null;
						break;
					case TouchPhase.Moved:
//						if(firstTouchedObject == null)
//						{
//							firstTouchedObject = touchedObject;
//							firstTouchedObject.OnTouchBegan();
//							touch2Watch = currTouch;
//						}
						if(firstTouchedObject != null  && firstTouchedObject == touchedObject)
						{
							firstTouchedObject.OnTouchMoved();
						}
						break;
					case TouchPhase.Stationary:
						if(firstTouchedObject != null && firstTouchedObject == touchedObject)
						{
							firstTouchedObject.OnTouchStayed();
						}
						break;
					}
				}
				
				//on anywhere
//				else if (touchedObject != null && touch2Watch == currTouch && firstTouchedObject != touchedObject)

				//alternative attempt to get on anywhere working correctly
				else if (touch2Watch == currTouch && firstTouchedObject != touchedObject && firstTouchedObject != null)
				{
					switch(Input.GetTouch(i).phase)
					{
					case TouchPhase.Began:
						firstTouchedObject.OnTouchBeganAnywhere();
						touch2Watch = currTouch;
						break;
					case TouchPhase.Ended:
						firstTouchedObject.OnTouchEndedAnywhere();
						firstTouchedObject = null;
						touchedObject = null;
						break;
					case TouchPhase.Moved:
						firstTouchedObject.OnTouchMovedAnywhere();
						break;
					case TouchPhase.Stationary:
						firstTouchedObject.OnTouchStayedAnywhere();
						break;
					}
				}
			}
		} else if(!isTouchDevice){
		
			//this is for mouse input
			if(touchedObject != null && !clicked && Input.GetMouseButtonDown(0))
			{
				firstClickedObject = touchedObject;
				firstClickedObject.OnTouchBegan();
				clicked = true;
			}
			
			if(clicked && Input.GetMouseButtonUp(0) && firstClickedObject == touchedObject)
			{
				firstClickedObject.OnTouchEnded();
				clicked = false;
				firstClickedObject = null;
			}
			
			if(clicked && Input.GetMouseButton(0) && firstClickedObject == touchedObject)
			{			
				firstClickedObject.OnTouchMoved();
			}


			//this is for mouse input where you've dragged off an object
			if(firstClickedObject != touchedObject && Input.GetMouseButtonDown(0))
			{
				firstClickedObject.OnTouchBeganAnywhere();
				clicked = true;
			}
			
			if(clicked && firstClickedObject != touchedObject && Input.GetMouseButtonUp(0))
			{
				firstClickedObject.OnTouchEndedAnywhere();
				clicked = false;
				firstClickedObject = null;
				touchedObject = null;
			}
			
			if(firstClickedObject != touchedObject && Input.GetMouseButton(0) && firstClickedObject != null)
			{			
				firstClickedObject.OnTouchMovedAnywhere();
			}
		}
	}
}