﻿public interface iInputManager {
	// remove functions you know that you aren't going to use
	// void OnNoTouches();
	void OnTouchBegan();
	void OnTouchEnded();
	void OnTouchMoved();
	void OnTouchStayed();
	void OnTouchBeganAnywhere();
	void OnTouchEndedAnywhere();
	void OnTouchMovedAnywhere();
	void OnTouchStayedAnywhere();
}