﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Prime31;

public class AnimButtonLeaderboard : MonoBehaviour, iInputManager {

	private GameObject buttonLeaderboardSprite;
	private Animator buttonLeaderboardAnim;
	private int buttonLeaderboardAnimState;
	private GameObject buttonLeaderboardSound1;
	private GameObject buttonLeaderboardSound2;
	private bool movedOff;

	void Start ()
	{
		buttonLeaderboardSprite = GameObject.Find("ButtonLeaderboardSprite");
		buttonLeaderboardAnim = buttonLeaderboardSprite.GetComponent<Animator>();
		buttonLeaderboardSound1 = GameObject.Find ("ButtonLeaderboardSound1");
		buttonLeaderboardSound2 = GameObject.Find ("ButtonLeaderboardSound2");
		movedOff = false;
	}

	#region iInputManager implementation

	public void OnTouchStayed ()
	{
//		throw new System.NotImplementedException ();
	}

	public void OnTouchBeganAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}

	public void OnTouchStayedAnywhere ()
	{
//		throw new System.NotImplementedException ();
	}

	public void OnTouchMovedAnywhere ()
	{
		if (!movedOff) 
		{
			movedOff = true;
			buttonLeaderboardSound2.audio.Play ();
		}

		buttonLeaderboardAnimState = 3;
		buttonLeaderboardAnim.SetInteger("buttonStates", buttonLeaderboardAnimState);
	}

	public void OnTouchMoved ()
	{
		if(movedOff)
		{
			buttonLeaderboardSound1.audio.Play ();
			movedOff = false;
		}

		if(buttonLeaderboardAnimState != 2)
		{
			buttonLeaderboardAnimState = 2;
			buttonLeaderboardAnim.SetInteger("buttonStates", buttonLeaderboardAnimState);
		}	
	}
	
	public void OnTouchBegan()
	{
		if(movedOff)
		{
			movedOff = false;
		}

		buttonLeaderboardSound1.audio.Play ();
		buttonLeaderboardAnimState = 2;
		buttonLeaderboardAnim.SetInteger("buttonStates", buttonLeaderboardAnimState);
	}

	public void OnTouchEnded()
	{
		if(movedOff)
		{
			movedOff = false;
		}

		buttonLeaderboardSound2.audio.Play ();
		buttonLeaderboardAnimState = 3;
		buttonLeaderboardAnim.SetInteger("buttonStates", buttonLeaderboardAnimState);
		#if UNITY_IPHONE
		GameCenterBinding.showLeaderboardWithTimeScope( GameCenterLeaderboardTimeScope.AllTime );
		#endif

		#if UNITY_ANDROID
		PlayGameServices.showLeaderboard("CgkIoMLlsYoFEAIQAA", GPGLeaderboardTimeScope.AllTime);
		#endif
	}

	public void OnTouchEndedAnywhere()
	{
		buttonLeaderboardSound2.audio.Play ();
	}

	#endregion

}
