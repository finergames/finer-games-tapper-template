﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Prime31;

public class FlurryManager : MonoBehaviour {

	#if UNITY_IPHONE

	private bool isAdDisplayed;

	// Use this for initialization
	void Start () {
		FlurryAnalytics.startSession( "8WHWSXG5WJYG3C684PJ9" );
		FlurryAds.enableAds( true );
		FlurryAds.fetchAdForSpace( "BannerBottom", FlurryAdSize.Bottom );
		isAdDisplayed = false;
	}

	void Update(){
		if(!isAdDisplayed){
			var isAvailable = FlurryAds.isAdAvailableForSpace( "BannerBottom", FlurryAdSize.Bottom );
			if(isAvailable){
				FlurryAds.displayAdForSpace( "BannerBottom", FlurryAdSize.Bottom );
				isAdDisplayed = true;
			}
		}
	}

	#endif

}
//	void OnGUI()
//	{
//		if( GUILayout.Button( "Start Flurry Session" ) )
//		{
//			// replace with your Flurry Key!!!
//			FlurryAnalytics.startSession( "8WHWSXG5WJYG3C684PJ9" );
//		}
//		
//		if( GUILayout.Button( "Enable Ads" ) )
//		{
//			FlurryAds.enableAds( true );
//		}
//		
//		
//		if( GUILayout.Button( "Fetch Ads" ) )
//		{
//			FlurryAds.fetchAdForSpace( "BannerBottom", FlurryAdSize.Bottom );
//		}
//
//		if( GUILayout.Button( "Show Ad on Bottom" ) )
//		{
//			FlurryAds.displayAdForSpace( "BannerBottom", FlurryAdSize.Bottom );
//		}
//		
//		if( GUILayout.Button( "Remove Ad" ) )
//		{
//			FlurryAds.removeAdFromSpace( "BannerBottom" );
//		}
//	}
