﻿using UnityEngine;
using System.Collections;

public class GameOverHighScore : MonoBehaviour {

	void Start () 
	{
		Grid.scoreManager.gameOverHighScore = Grid.scoreManager.highScore;
	}
	
	void Update () 
	{
		this.guiText.text = "" + Grid.scoreManager.gameOverHighScore;
	}

	public void HighScoreCounter()
	{
		if(Grid.scoreManager.gameOverHighScore < Grid.scoreManager.highScore)
		{
			Grid.scoreManager.gameOverHighScore = Grid.scoreManager.highScore;
		}

		//yield return 0;
	}
}
