﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

	public GameObject music;
//	private float volume, fadeSpeed;
	private float fadeSpeed;
	private bool audioPlaying;

	// Use this for initialization
	void Start ()
	{
		fadeSpeed = 0.05f;
//		volume = audio.volume;
		audioPlaying = false;
	}

	void Update ()
	{
		if(!music)
		{
			music = GameObject.FindGameObjectWithTag("GameMusic");
		}

		if(Application.loadedLevelName == "MainMenu")
		{
			if(!audioPlaying)
			{
				audio.Play();
				audioPlaying = true;
			}
		}
	}

//	public void FadeOutMusic()
//	{
//		audio.volume -= Mathf.Lerp(volume, 0.0f, fadeSpeed * Time.deltaTime);
//	}

	public IEnumerator FadeOutMusic()
	{
		
		//yield return new WaitForSeconds(2.0f);
		
		while (audio.volume > 0.0f) {
			//audio.volume -= 0.001f;
			audio.volume = Mathf.Lerp(audio.volume, 0.0f, fadeSpeed * Time.deltaTime);

			if(audio.volume < 0.01f){
				audio.enabled = false;
			}

			yield return 0;
		}
//		audio.volume = 0.0f;
//
//		yield return 0;


	}
}
