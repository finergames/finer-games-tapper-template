﻿using UnityEngine;
using System.Collections;

public class GameOverPoints : MonoBehaviour {

	private GameOverHighScore gameOverHighScore;

	void Start () 
	{
		gameOverHighScore = GameObject.Find ("GameOverHighScoreDisplay").GetComponent<GameOverHighScore> ();
	}
	
	void Update () 
	{
		this.guiText.text = "" + Grid.scoreManager.gameOverPoints;
	}

	public IEnumerator ScoreCounter()
	{
		yield return new WaitForSeconds(2.0f);
		
		while (Grid.scoreManager.gameOverPoints < Grid.scoreManager.totalPoints) {
			Grid.scoreManager.gameOverPoints++;
			yield return 0;
		}

		gameOverHighScore.HighScoreCounter ();
	}
}
