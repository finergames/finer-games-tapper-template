﻿using UnityEngine;
using System.Collections;

public class FlickerOverlay : MonoBehaviour {

	private GameObject flickerOverlay;

	IEnumerator DestroyFlagExplosion()
	{
		yield return new WaitForSeconds(0.5f);
		flickerOverlay.SetActive (false);
	}
	
	void Start () {
		flickerOverlay = GameObject.Find ("FlickerOverlay");
		StartCoroutine(DestroyFlagExplosion());
	}
}
