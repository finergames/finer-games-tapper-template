using UnityEngine;
using System.Collections;

public class AnimButtonPlay : MonoBehaviour, iInputManager {

	private GameObject buttonPlaySprite;
	private Animator buttonPlayAnim;
	private int buttonPlayAnimState;
	private GameObject buttonPlaySound1;
	private GameObject buttonPlaySound2;
	private bool movedOff;

	void Start ()
	{
		buttonPlaySprite = GameObject.Find("ButtonPlaySprite");
		buttonPlayAnim = buttonPlaySprite.GetComponent<Animator>();
		buttonPlaySound1 = GameObject.Find ("ButtonPlaySound1");
		buttonPlaySound2 = GameObject.Find ("ButtonPlaySound2");
		movedOff = false;
	}

	#region iInputManager implementation

	public void OnTouchStayed ()
	{
//		throw new System.NotImplementedException ();
	}

	public void OnTouchBeganAnywhere ()
	{
//		throw new System.NotImplementedException ();	
	}

	public void OnTouchStayedAnywhere ()
	{
//		throw new System.NotImplementedException ();	
	}

	public void OnTouchMovedAnywhere ()
	{
		if (!movedOff) 
		{
			movedOff = true;
			buttonPlaySound2.audio.Play ();
		}

		buttonPlayAnimState = 3;
		buttonPlayAnim.SetInteger("buttonStates", buttonPlayAnimState);
	}

	public void OnTouchMoved ()
	{
		if(movedOff)
		{
			buttonPlaySound1.audio.Play ();
			movedOff = false;
		}

		if(buttonPlayAnimState != 2)
		{
			buttonPlayAnimState = 2;
			buttonPlayAnim.SetInteger("buttonStates", buttonPlayAnimState);
		}
	}
	
	public void OnTouchBegan()
	{
		if(movedOff)
		{
			movedOff = false;
		}

		buttonPlaySound1.audio.Play ();
		buttonPlayAnimState = 2;
		buttonPlayAnim.SetInteger("buttonStates", buttonPlayAnimState);
	}

	public void OnTouchEnded()
	{
		if(movedOff)
		{
			movedOff = false;
		}

		buttonPlaySound2.audio.Play ();
		buttonPlayAnimState = 3;
		buttonPlayAnim.SetInteger("buttonStates", buttonPlayAnimState);

		Grid.levelManager.sceneToLoad = "MainLevel";
		Grid.levelManager.sceneEnding = true;
	}

	public void OnTouchEndedAnywhere()
	{
		buttonPlaySound2.audio.Play ();
	}

	#endregion

}
