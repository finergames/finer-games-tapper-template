﻿using UnityEngine;
using System.Collections;

public class StageDimensions : MonoBehaviour {

	public static Vector3 stageDimensions;
	private GameObject wallLeft, wallRight, wallTop, wallBottom;

	// Use this for initialization
	void Start () 
	{
		stageDimensions = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, 0));

		wallLeft = GameObject.Find ("WallLeft");
		wallRight = GameObject.Find ("WallRight");
		wallTop = GameObject.Find ("WallTop");
		wallBottom = GameObject.Find ("WallBottom");

		Vector3 wallLeftCollider = wallLeft.GetComponent<Collider2D>().bounds.size;
		wallLeft.transform.position = new Vector3 (0.0f - stageDimensions.x - (wallLeftCollider.x/2.0f), 0.0f, 0.0f);

		Vector3 wallRightCollider = wallRight.GetComponent<Collider2D>().bounds.size;
		wallRight.transform.position = new Vector3 (0.0f + stageDimensions.x + (wallRightCollider.x/2.0f), 0.0f, 0.0f);

		Vector3 wallTopCollider = wallTop.GetComponent<Collider2D>().bounds.size;
		wallTop.transform.position = new Vector3 (0.0f, 0.0f + stageDimensions.y + (wallTopCollider.y/2.0f), 0.0f);

		Vector3 wallBottomCollider = wallBottom.GetComponent<Collider2D>().bounds.size;
		wallBottom.transform.position = new Vector3 (0.0f, 0.0f - stageDimensions.y - (wallBottomCollider.y/2.0f), 0.0f);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
