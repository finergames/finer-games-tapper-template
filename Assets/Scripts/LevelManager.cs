﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	private float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.
	private SpriteRenderer blackTexture;
	
	public bool sceneStarting = true;      // Whether or not the scene is still fading in.
	public bool sceneEnding = false;      // Whether or not the scene is still fading out.
	public string sceneToLoad;
	
	void Awake ()
	{
		// Set the texture so that it is the the size of the screen and covers it.
//		guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
		blackTexture = this.GetComponent<SpriteRenderer> ();
	}

	void Start ()
	{
		if(Application.loadedLevelName == "__preEverythingScene"){
			Application.LoadLevel("SplashScreen");
		}
	}
	
	
	void Update ()
	{
		// If the scene is starting...
		if(sceneStarting)
			// ... call the StartScene function.
			StartScene();

		if (sceneEnding)
			EndScene ();
	}
	
	
	void FadeToClear ()
	{
		// Lerp the colour of the texture between itself and transparent.
		blackTexture.color = Color.Lerp(blackTexture.color, Color.clear, fadeSpeed * Time.deltaTime);
	}
	
	
	void FadeToBlack ()
	{
		// Lerp the colour of the texture between itself and black.
		blackTexture.color = Color.Lerp(blackTexture.color, Color.black, fadeSpeed * Time.deltaTime);
	}
	
	
	void StartScene ()
	{
		// Fade the texture to clear.
		FadeToClear();
		
		// If the texture is almost clear...
		if(blackTexture.color.a <= 0.01f)
		{
			// ... set the colour to clear and disable the sprite renderer.
			blackTexture.color = Color.clear;
			blackTexture.enabled = false;
			
			// The scene is no longer starting.
			sceneStarting = false;
		}
	}
	
	
	public void EndScene ()
	{
		// Make sure the texture is enabled.
		blackTexture.enabled = true;
		
		// Start fading towards black.
		FadeToBlack();
		//StartCoroutine (Grid.musicManager.FadeOutMusic);
		if (Application.loadedLevelName == "MainMenu") 
		{
			Grid.musicManager.StartCoroutine (Grid.musicManager.FadeOutMusic ());
		}

		// If the screen is almost black...
		if (blackTexture.color.a >= 0.49f) {
			blackTexture.color = Color.black;
			sceneEnding = false;
			// ... reload the level.
			Application.LoadLevel (sceneToLoad);
			sceneStarting = true;
		}
	}
}
